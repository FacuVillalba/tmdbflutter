import 'package:flutter/material.dart';
import 'screeens/main_tab_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'The Movie Database Flutter',
      theme: ThemeData(
        primarySwatch: Colors.orange,
      ),
      home: MainTabScreen(),
    );
  }
}


