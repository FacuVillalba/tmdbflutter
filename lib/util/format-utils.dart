String printList(List<String> list) {
  String result = '';
  list.forEach((name) => result += name + ", ");
  return result.length > 2 ? result.substring(0, result.length - 2) + "." : '';
}
