import 'package:http/http.dart' as http;
import 'package:the_movie_db_flutter_client/data/Movie.dart';
import 'dart:convert';

class TMDbRepository {
  static final String apiUrl = 'https://api.themoviedb.org';
  static final String apiVersion = '3';
  static final String apiKeyParam = '?api_key=28220c2e66b7eb205cb6de59f35f0939';
  static final String pageParam = '&page=';
  static final String appendCreditsAndVideos =
      '&append_to_response=credits,videos';

  static final String baseUrl = 'http://image.tmdb.org/t/p/';

  static final String fileSize = 'w500';
  static final String baseImageUrl = baseUrl + fileSize;
  static final String baseURL = apiUrl + '/' + apiVersion;

  static final String youtubeTrailerPrefix = 'https://www.youtube.com/watch?v=';
  static final String director = 'Director';
  static final String writer = 'Writing';

  static final String youTubeAPIKey = 'AIzaSyDVZMbvs8G75lYlv6pqAtTvIu5yG7ds6Mg';

  static Future<List<Movie>> discoverMovies({int page = 1}) async {
    List<Movie> list = [];
    String url = baseURL +
        '/discover/movie/' +
        apiKeyParam +
        pageParam +
        page.toString();
    final response = await http.get(url);
    if (response.statusCode == 200) {
      Map result = json.decode(response.body);
      if (result.containsKey('results') && result['results'].length > 0)
        (result['results']).forEach((movie) => list.add(Movie.fromJson(movie)));
    }
    return list;
  }

  static Future<Movie> fetchMovieDetails(Movie movie) async {
    if (!movie.areDetailsLoaded) {
      String url = baseURL +
          '/movie/' +
          movie.id.toString() +
          apiKeyParam +
          appendCreditsAndVideos;
      final response = await http.get(url);
      if (response.statusCode == 200) {
        Map result = json.decode(response.body);
        movie.homepage = result['homepage'];
        movie.runtime = result['runtime'];
        Stream.fromIterable(result['genres'])
            .forEach((i) => movie.genres.add(i['name']));
        movie.trailerURL =
            youtubeTrailerPrefix + result['videos']['results'][0]['key'];
        var credits = result['credits'];
        for (var i = 0; i < 5; i++) {
          movie.cast.add(credits['cast'][i]['name']);
        }
        Stream.fromIterable(credits['crew'])
            .where((n) => n['job'].toString() == director)
            .forEach((i) => movie.director.add(i['name']));
        Stream.fromIterable(credits['crew'])
            .where((n) => n['department'].toString() == writer)
            .forEach((i) => movie.writers.add(i['name']));
        movie.areDetailsLoaded = true;
      }
    }
    return movie;
  }

  //TODO config object
  static Future<List<Movie>> fetchConfiguration() async {
    List<Movie> list = [];
    final response = await http.get(baseURL + '/configuration/' + apiKeyParam);
    if (response.statusCode == 200) {}
    return list;
  }

  static String getImage(String posterPath) {
    return baseImageUrl + posterPath;
  }
}
