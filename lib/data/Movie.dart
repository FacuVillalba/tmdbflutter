class Movie {
  int id;
  String posterPath;
  String overview;
  String releaseDate;
  String title;
  String backdropPath;
  String trailerURL;
  double popularity;
  int voteCount;
  double voteAverage;
  int runtime;

  //TODO refactor
  bool areDetailsLoaded = false;

  String homepage;
  List<String> genres = [];

  List<String> director = [];
  List<String> writers = [];
  List<String> cast = [];

  Movie.fromJson(Map json)
      : title = json['title'],
        posterPath = json['poster_path'],
        overview = json['overview'],
        id = json['id'],
        voteAverage = json['vote_average'].toDouble(),
        releaseDate = json['release_date'],
        popularity = json['popularity'],
        voteCount = json['vote_count'],
        backdropPath = json['backdrop_path'];
}
