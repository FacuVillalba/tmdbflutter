import 'package:flutter/material.dart';
import 'package:the_movie_db_flutter_client/data/Movie.dart';
import 'package:the_movie_db_flutter_client/util/TMDbRepository.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_youtube/flutter_youtube.dart';

enum Options { webSite, trailer }

class OptionsWidget extends StatelessWidget {
  final Movie movie;

  OptionsWidget(this.movie, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton(
      onSelected: _onSelected,
      icon: Icon(Icons.more_horiz),
      itemBuilder: (BuildContext context) {
        return <PopupMenuEntry<Options>>[
          const PopupMenuItem<Options>(
            value: Options.trailer,
            child: Text('Official trailer'),
          ),
          const PopupMenuItem<Options>(
            value: Options.webSite,
            child: Text('Open web site'),
          ),
        ];
      },
    );
  }

  _onSelected(Options option) {
    switch (option) {
      case Options.webSite:
        _launchURL();
        break;
      case Options.trailer:
        _playTrailer();
        break;
    }
  }

  _launchURL() async {
    if (await canLaunch(movie.homepage)) {
      await launch(movie.homepage);
    } else {
      throw 'Could not launch $movie.homepage';
    }
  }

  _playTrailer() {
    FlutterYoutube.playYoutubeVideoByUrl(
      apiKey: TMDbRepository.youTubeAPIKey,
      videoUrl: movie.trailerURL,
      autoPlay: true,
      fullScreen: true,
    );
  }
}
