import 'package:flutter/material.dart';

class VoteAverage extends StatelessWidget {
  final double votes;

  VoteAverage(this.votes, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double normalizedVotes = votes / 2;
    int roundedVotes = (normalizedVotes).round();
    List<Widget> stars = [
      Icon(Icons.star_border),
      Icon(Icons.star_border),
      Icon(Icons.star_border),
      Icon(Icons.star_border),
      Icon(Icons.star_border),
    ];
    for (var i = 0; i < 5; i++) {
      if (i < roundedVotes) {
        stars[i] = Icon(Icons.star);
      } else {
        if (i == roundedVotes && (normalizedVotes - i) >= 0.25) {
          stars[i] = Icon(Icons.star_half);
          break;
        }
      }
    }
    return Row(children: stars);
  }
}
