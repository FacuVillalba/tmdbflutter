import 'package:flutter/material.dart';
import 'package:the_movie_db_flutter_client/screeens/search.dart';
import 'package:the_movie_db_flutter_client/screeens/discover.dart';

class MainTabScreen extends StatefulWidget {
  MainTabScreen({Key key}) : super(key: key);

  @override
  _MainTabScreenState createState() => _MainTabScreenState();
}

class _MainTabScreenState extends State<MainTabScreen>
    with AutomaticKeepAliveClientMixin<MainTabScreen> {
  int _selectedScreenIndex = 0;

  static final Key _keyDiscoverScreen = PageStorageKey('DiscoverScreen');
  static final Key _keySearchScreen = PageStorageKey('SearchScreen');
  static final Key _keySettingsScreen = PageStorageKey('Text');

  @override
  bool get wantKeepAlive => true;

  final _screens = [
    DiscoverScreen(key: _keyDiscoverScreen),
    SearchScreen(key: _keySearchScreen),
    Text('TODO', key: _keySettingsScreen),
  ];

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      body: Center(
        child: _screens.elementAt(_selectedScreenIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: Icon(Icons.local_movies), title: Text('Discover')),
          BottomNavigationBarItem(
              icon: Icon(Icons.search), title: Text('Search')),
          BottomNavigationBarItem(
              icon: Icon(Icons.settings), title: Text('Settings')),
        ],
        currentIndex: _selectedScreenIndex,
        onTap: _onItemTapped,
      ),
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedScreenIndex = index;
    });
  }
}
