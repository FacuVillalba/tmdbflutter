import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:carousel/carousel.dart';
import 'package:the_movie_db_flutter_client/data/Movie.dart';
import 'package:the_movie_db_flutter_client/util/TMDbRepository.dart';
import 'package:the_movie_db_flutter_client/util/format-utils.dart';
import 'package:the_movie_db_flutter_client/widgets/options_widget.dart';
import 'package:the_movie_db_flutter_client/widgets/vote_average_widget.dart';

class MovieDetails extends StatefulWidget {
  final Movie movie;

  MovieDetails(this.movie, {Key key}) : super(key: key);

  @override
  _MovieDetailsState createState() => _MovieDetailsState();
}

class _MovieDetailsState extends State<MovieDetails> {
  bool _isFavorite = false;
  final GlobalKey _keyDetailsPanel = GlobalKey();
  final GlobalKey _keyHeader = GlobalKey();

  double _blankSpace = 350;

  @override
  void initState() {
    SchedulerBinding.instance
        .addPostFrameCallback((_) => _setBlankSpace(context));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: CustomScrollView(
          slivers: <Widget>[
            SliverAppBar(
              backgroundColor: Colors.white,
              expandedHeight: 230.0,
              floating: false,
              flexibleSpace:
                  FlexibleSpaceBar(background: _buildCarouselWidget()),
            ),
            SliverPersistentHeader(
              pinned: true,
              delegate: _SliverPersistentHeaderDelegate(
                  widget.movie, _keyHeader, _toggleFavorite, _isFavorite),
            ),
            SliverList(
              delegate: SliverChildListDelegate(
                [
                  _buildMovieDetailsPanel(),
                  SizedBox(
                    height: _blankSpace,
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  ///Calculate remaining space to let sliver list to fully compress
  _setBlankSpace(BuildContext context) {
    RenderBox renderBoxHeader = _keyHeader.currentContext.findRenderObject();
    RenderBox renderBoxDetails =
        _keyDetailsPanel.currentContext.findRenderObject();
    double headerSize = renderBoxHeader.size.height;
    double detailsSize = renderBoxDetails.size.height;
    //TODO review -20 to avoid text overlap
    double blankSpace = context.size.height - 20 - headerSize - detailsSize;
    setState(() => _blankSpace = blankSpace);
  }

  _buildCarouselWidget() {
    return Carousel(
      animationDuration: Duration(seconds: 1),
      displayDuration: Duration(seconds: 5),
      children: [
        NetworkImage(TMDbRepository.getImage(widget.movie.backdropPath)),
        NetworkImage(TMDbRepository.getImage(widget.movie.posterPath)),
      ]
          .map((netImage) => Image(
                image: netImage,
                fit: BoxFit.fitHeight,
              ))
          .toList(),
    );
  }

  _buildMovieDetailsPanel() {
    return Padding(
      key: _keyDetailsPanel,
      padding: const EdgeInsets.only(left: 10, right: 10, top: 10),
      child: Column(
        children: <Widget>[
          Text(widget.movie.overview),
          Padding(
            padding: const EdgeInsets.only(top: 15.0),
            child: GridView.count(
              primary: false,
              shrinkWrap: true,
              childAspectRatio: 2,
              crossAxisCount: 2,
              children: <Widget>[
                Text(
                  "Director",
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
                ),
                Text(printList(widget.movie.director)),
                Text(
                  "Cast",
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
                ),
                Text(printList(widget.movie.cast)),
                Text(
                  "Writers",
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
                ),
                Text(printList(widget.movie.writers)),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void _toggleFavorite() {
    setState(() => _isFavorite ? _isFavorite = false : _isFavorite = true);
  }
}

class _SliverPersistentHeaderDelegate extends SliverPersistentHeaderDelegate {
  GlobalKey _keyHeader;
  Movie movie;
  bool _isFavorite;
  bool longName = true;
  Function _favoritePressed;

  _SliverPersistentHeaderDelegate(
      this.movie, this._keyHeader, this._favoritePressed, this._isFavorite) {
    longName = movie.title.length > 32;
  }

  @override
  bool shouldRebuild(_SliverPersistentHeaderDelegate oldDelegate) => true;

  //TODO make dimensions dynamic
  @override
  double get maxExtent => longName ? 132 : 103;

  @override
  double get minExtent => longName ? 132 : 103;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Container(
      key: _keyHeader,
      padding: const EdgeInsets.symmetric(horizontal: 10.0),
      color: Colors.white,
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              VoteAverage(movie.voteAverage),
              Expanded(
                child: Container(),
              ),
              _buildFavouriteWidget(),
              OptionsWidget(movie),
            ],
          ),
          Padding(
            padding: EdgeInsets.only(bottom: 8),
            child: _buildHeaderInfoWidget(),
          ),
        ],
      ),
    );
  }

  _buildFavouriteWidget() {
    return IconButton(
      icon: Icon(
        _isFavorite ? Icons.favorite : Icons.favorite_border,
        color: _isFavorite ? Colors.red : Colors.orange,
      ),
      onPressed: _favoritePressed,
    );
  }

  _buildHeaderInfoWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Text(movie.title, style: TextStyle(fontSize: 25.0)),
        Text(movie.releaseDate, style: TextStyle(fontSize: 15.0)),
      ],
    );
  }
}
