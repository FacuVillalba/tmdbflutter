import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:the_movie_db_flutter_client/data/Movie.dart';
import 'package:the_movie_db_flutter_client/util/TMDbRepository.dart';
import 'package:transparent_image/transparent_image.dart';

import 'package:the_movie_db_flutter_client/screeens/movie_details.dart';

class DiscoverScreen extends StatefulWidget {
  DiscoverScreen({Key key}) : super(key: key);

  @override
  _DiscoverScreenState createState() => _DiscoverScreenState();
}

class _DiscoverScreenState extends State<DiscoverScreen>
    with AutomaticKeepAliveClientMixin<DiscoverScreen> {
  Future<List<Movie>> movies;
  Future<List<Movie>> movies2;
  Future<List<Movie>> movies3;

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
    movies = TMDbRepository.discoverMovies();
    movies2 = TMDbRepository.discoverMovies(page: 2);
    movies3 = TMDbRepository.discoverMovies(page: 3);
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Discover'),
      ),
      body: ListView(
        children: <Widget>[
          _categoryDivider('Trending News'),
          _buildMoviesPanel(movies),
          _categoryDivider('Popular Movies'),
          _buildMoviesPanel(movies2),
          _categoryDivider('Popular Tv Shows'),
          _buildMoviesPanel(movies3),
        ],
      ),
    );
  }

  _buildMoviesPanel(Future<List<Movie>> movies) {
    return Container(
      height: 250,
      child: FutureBuilder<List<Movie>>(
        future: movies,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.hasData) {
            return _createListView(context, snapshot);
          }
          return _buildLoadingView();
        },
      ),
    );
  }

  _createListView(BuildContext context, AsyncSnapshot snapshot) {
    List<Movie> values = snapshot.data;
    return ListView.builder(
      scrollDirection: Axis.horizontal,
      itemCount: values.length,
      itemBuilder: (BuildContext context, int index) {
        String imageUrl =
            TMDbRepository.baseImageUrl + values.elementAt(index).posterPath;
        return InkWell(
          //TODO refactor and add spinner for http request
          onTap: () {
            _onLoading();
            Future<Movie> movieWithDetails =
                TMDbRepository.fetchMovieDetails(values.elementAt(index));
            movieWithDetails.then((movie) {
              Navigator.pop(context);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => MovieDetails(movie)),
              );
            });
          },
          child: Container(
            width: 180.0,
            child: FadeInImage.memoryNetwork(
              image: imageUrl,
              placeholder: kTransparentImage,
            ),
          ),
        );
      },
    );
  }

  _buildLoadingView() {
    return ListView(
      shrinkWrap: true,
      scrollDirection: Axis.horizontal,
      children: <Widget>[
        _buildPlaceholder(),
        _buildPlaceholder(),
        _buildPlaceholder(),
        _buildPlaceholder(),
        _buildPlaceholder(),
      ],
    );
  }

  _buildPlaceholder() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Shimmer.fromColors(
        baseColor: Colors.grey[300],
        highlightColor: Colors.grey[200],
        child: Container(
          color: Colors.white,
          height: 250,
          width: 180,
        ),
      ),
    );
  }

  _categoryDivider(String text) {
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: Text(
        text,
        style: TextStyle(
          fontSize: 20,
        ),
      ),
    );
  }

  void _onLoading() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Column(
            children: <Widget>[
              CircularProgressIndicator(),
              Text("Loading"),
            ],
          ),
        );
      },
    );
  }
}
